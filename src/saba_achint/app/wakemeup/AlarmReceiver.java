package saba_achint.app.wakemeup;

import saba_achint.app.wakemeup.R;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.widget.Toast;

public class AlarmReceiver extends BroadcastReceiver {
	MediaPlayer mp2 = new MediaPlayer();
	@Override
	public void onReceive(Context arg0, Intent arg1) {
		Toast.makeText(arg0, "Feel the force!!", Toast.LENGTH_SHORT).show();
		Intent iAlarm = new Intent( arg0, Easy.class ); 
        iAlarm.addFlags(Intent.FLAG_FROM_BACKGROUND); 
        iAlarm.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); 
        iAlarm.putExtra("Ringtone", 
                Uri.parse("getResources().getResourceName(R.raw.play)"));
        
        mp2 = MediaPlayer.create(arg0, R.raw.play);      
        mp2.setLooping(true);   
        mp2.start();
        
        arg0.startActivity(iAlarm); 
		
	}
}
